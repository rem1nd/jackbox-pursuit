echo -e "\e[34mjackbox-pursuit! a dumb script to find valid rooms"
echo -e "\e[32msearching for rooms...."
for run in {1..5000}
do
p="$(head /dev/urandom | tr -dc A-Z | head -c 4 ; echo '')"
if curl -s "https://ecast.jackboxgames.com/room/$p?userId=f" | grep roomid >> log.txt
then
    echo "$p is a valid room!"
    grep $p log.txt
fi
done