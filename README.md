# jackbox-pursuit


![alt text](jackbox-pursuit.jpg "jackbox-pursuit")

Script that attempts to find a valid jackbox room. You can then join the room by using the roomid.

![alt text](running.PNG "script running")

**Caution:**
This script sends 5,000 web requests to jackbox by the default configuration. Please customize your script and be nice to jackbox's servers.